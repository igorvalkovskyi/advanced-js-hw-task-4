fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {
        const ulElement = document.createElement("ul")
        console.log(films);

        films.forEach(films => {
            const liElement = document.createElement("li")
            liElement.innerHTML = `номер eпізода : ${films.episodeId}
             назва фільму : ${films.name}
            короткий зміст : ${films.openingCrawl} `
            document.body.append(ulElement)
            ulElement.appendChild(liElement)

            films.characters.forEach(actor => {
                fetch(actor).then(person => person.json()).then(person => {
                    console.log(person);
                    const p = document.createElement("p");
                    for (key in person) {
                        p.innerHTML += `${key}: ${person[key]}`
                    }
                    document.body.append(p)
                })


            })

        })


    })
